'''
Anvio_refine_bin: short pipeline to refine a bin (set of contigs) using the Anvio 
software.

Copyright (C) 2016, Antonio Diaz Tula

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys, os
from config import *

srcPath = os.path.join(contigsDir, contigsFile)
outPath = os.path.join(outDir, contigsFile)
print "Processing %s"%srcPath
# Create index file
os.system("bwa index %s"%(srcPath))
# Map reads to contigs using bwa
c = "bwa mem %s %s %s > %s"%(srcPath, readsForward, readsBackward, outPath.replace(".fasta", "")+".sam")
print "Running : ", c
os.system(c)
# Converting SAM to BAM
c = "samtools view -Sb %s > %s"%(outPath.replace(".fasta", "")+".sam", outPath.replace(".fasta", "")+".bam")
print "Running : ", c
os.system(c)
print "Cleaning"
os.remove(outPath.replace(".fasta", "")+".sam")
for ext in ["amb", "ann", "bwt", "pac", "sa"]:
    os.remove(srcPath+"."+ext)
