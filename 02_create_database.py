import sys, os
from config import *

srcPath = os.path.join(contigsDir, contigsFile)
outPath = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
print "Creating anvio database"
c = "anvi-gen-contigs-database -f %s -o %s"%(srcPath, outPath)
print "Running: ", c
os.system(c)
