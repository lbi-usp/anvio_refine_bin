import sys, os
from config import *

bamPath   = os.path.join(outDir, contigsFile.replace(".fasta", "_sorted.bam"))
contigsDb = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
profDir   = os.path.join(outDir, "profile")

print "Creating anvio profile"
c = "anvi-profile -i %s -c %s -S ReadsFrom%s --min-contig-length 1000 --output-dir %s"%(bamPath, contigsDb, contigsFile.replace("_sorted.bam", "").replace(".fasta", "").replace(".", ""), profDir)
print "Running: ", c
os.system(c)
