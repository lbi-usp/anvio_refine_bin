contigsDir    = "../test_datasets/anvio_test_data/contigs"
contigsFile   = "Bin015.fasta"
readsForward  = "../test_datasets/anvio_test_data/reads/ZLK00R00R1_Verrucomicrobia.fastq"
readsBackward = "../test_datasets/anvio_test_data/reads/ZLK00R00R2_Verrucomicrobia.fastq"
outDir        = "results"

minContigsLen = 1000
