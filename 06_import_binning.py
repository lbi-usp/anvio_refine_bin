import sys, os
from config import *
from createBinningTxt import *

srcPath     = os.path.join(contigsDir, contigsFile)
profilePath = os.path.join(outDir, "profile/PROFILE.db")
binFile     = os.path.join(outDir, contigsFile.replace(".fasta", ".txt"))
contigsDb   = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
collection  = contigsFile.replace(".fasta", "").replace(".", "")

createBinningFile(srcPath, binFile)

c = "anvi-import-collection %s -p %s -c %s -C %s --contigs-mode" %(binFile, profilePath, contigsDb, collection)
print "Running: ", c
os.system(c)
