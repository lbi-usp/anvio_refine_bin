import sys, os
from config import *

profilePath = os.path.join(outDir, "profile/PROFILE.db")
contigsDb   = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
collection  = contigsFile.replace(".fasta", "").replace(".", "")
summaryDir  = os.path.join(outDir, "summary")
c = "anvi-summarize -p %s -c %s -o %s -C %s"%(profilePath, contigsDb, summaryDir, collection)
print "Running: ", c
os.system(c)

