# Usando o Anvio para visualizar um bin #

Esse tutorial apresenta um conjunto de scripts escritos em Python para facilitar o uso da ferramenta [Anvio](http://merenlab.org/software/anvio/) para visualizar e refinar um bin (arquivo **.fasta** formado por um conjunto de contigs). O bin deve ser obtido por meio de algum software de clusterização, como MaxBin, Metabat, CONCOCT entre outros. 

O Anvio tem muitas opções como realizar a clusterização usando o CONCOCT, visualizar vários bins ao mesmo tempo, etc. Os scripts fornecidos aqui servem para visualizar **apenas** 1 bin, que pode então ser refinado usando as facilidades fornecidas pela interface interativa do Anvio e que não são explicadas aqui. Os passos descritos a seguir estão documentados no site do Anvio para workflow em metagenômica: http://merenlab.org/2016/06/22/anvio-tutorial-v2/. Os scripts apresentados a seguir visam simplificar a execução do workflow para 1 bin.

### Dependências ###

Os scripts apresentados nesse tutorial estão escritos em Python 2.7 e requerem dos seguintes softwares instalados no computador:

* [BWA](https://github.com/lh3/bwa)
* [samtools](http://www.htslib.org/download/)
* [Anvio](https://github.com/meren/anvio/) (usamos a versão 2.0.2)

### Scripts ###

A entrada consiste em um arquivo **.fasta** que representa um bin (um conjunto de contigs que muito provavelmente pertencem ao mesmo organismo) e que deve ser resultado da execução de um software de clusterização ou binning. Além disso, o Anvio requer o arquivo **.bam** com informação sobre o mapeamento dos reads no contig a ser visualizado. Se o usuário já tem o arquivo BAM então pode passar diretamente ao passo 2. Se o arquivo BAM não existir, então o usuário precisa ter os arquivos com os reads que foram usados para montar o contig. Essa informação é configurada no arquivo *config.py* como mostrado a seguir:


```
#!python
contigsDir    = "dir_with_contigs_file"
contigsFile   = "name_contigs_file"
readsForward  = "full_path_reads_forward"
readsBackward = "full_path_reads_backward"
outDir        = "results_dir"

```
Se o usuário já possui o arquivo BAM dos reads mapeados no contig então não precisa especificar os arquivos de reads e pode pular para o passo 2. Mas antes copie o arquivo BAM no diretório de resultados e renomeie-o *contigsFile.bam* tirando a extensão *.fasta* do contig original.

---

**Passo 1**. Criação do arquivo BAM: rodar o script *01_create_bam_files.py* o qual lê do arquivo *config.py* os caminhos para os arquivos com o os reads e contigs. No fim da execução do script, o diretório de saída (*outDir*) contem o arquivo BAM correspondente ao arquivo fasta de entrada (*contigsFile*).


```
#!python

import sys, os
from config import *

srcPath = os.path.join(contigsDir, contigsFile)
outPath = os.path.join(outDir, contigsFile)
print "Processing %s"%srcPath
# Create index file
os.system("bwa index %s"%(srcPath))
# Map reads to contigs using bwa
c = "bwa mem %s %s %s > %s"%(srcPath, readsForward, readsBackward, outPath.replace(".fasta", "")+".sam")
print "Running : ", c
os.system(c)
# Converting SAM to BAM
c = "samtools view -Sb %s > %s"%(outPath.replace(".fasta", "")+".sam", outPath.replace(".fasta", "")+".bam")
print "Running : ", c
os.system(c)
print "Cleaning"
os.remove(outPath.replace(".fasta", "")+".sam")
for ext in ["amb", "ann", "bwt", "pac", "sa"]:
    os.remove(srcPath+"."+ext)
```

---

**Passo 2**. Criar um banco de dados com os contigs existentes no arquivo **.fasta**, rodando o script *02_create_database.py*.

```
#!python
import sys, os
from config import *

srcPath = os.path.join(contigsDir, contigsFile)
outPath = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
print "Creating anvio database"
c = "anvi-gen-contigs-database -f %s -o %s"%(srcPath, outPath)
print "Running: ", c
os.system(c)
```

Após rodar esse script, o diretório de saída terá um arquivo **.db** com o mesmo nome do arquivo fasta.

---

**Passo 3**. Rodar hmm no arquivo de banco de dados. Para isso executar o script *03_run_hmm.py*

```
#!python

import sys, os
from config import *

outPath = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
print "Running hmms on the database"
c = "anvi-run-hmms -c %s --num-threads 4"%(outPath)
print "Running: ", c
os.system(c)

```

---

**Passo 4**. O próximo passo no workflow do Anvio consiste em ordenar o arquivo BAM. Para isso basta apenas rodar o script *04_sort_bam.py*, que criará o arquivo **contigsFile_sorted.bam** no diretório de saída, removendo a extensão *.fasta*.


```
#!python

import sys, os
from config import *

srcPath = os.path.join(outDir, contigsFile.replace(".fasta", ".bam"))
print "Sorting bam file"
c = "anvi-init-bam %s -o %s"%(srcPath, srcPath.replace(".bam", "_sorted.bam"))
print "Running: ", c
os.system(c)

```

---

** Passo 5**. Após ordenar o arquivo BAM, o próximo passo é a criação do perfil Anvio, para o qual precisamos do arquivo BAM ordenado assim como o banco de dados. Por simplicidade adotamos o diretório onde o perfil será criando como sendo *outDir/profile*, onde o Anvio colocará alguns arquivos do perfil. Também definimos o nome do perfil como sendo "ReadsFrom[contigsFile]" removendo a extensão *.fasta* e todos os pontos "." do nome do arquivo, mas esse nome não é usado (por enquanto). O script é:

```
#!python
import sys, os
from config import *

bamPath   = os.path.join(outDir, contigsFile.replace(".fasta", "_sorted.bam"))
contigsDb = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
profDir   = os.path.join(outDir, "profile")
print "Creating anvio profile"
c = "anvi-profile -i %s -c %s -S ReadsFrom%s --min-contig-length 1000 --output-dir %s"%(bamPath, contigsDb, contigsFile.replace("_sorted.bam", "").replace(".fasta", "").replace(".", ""), profDir)
print "Running: ", c
os.system(c)
```

---

**Passo 6**. Nesse ponto do workflow, o manual do Anvio realiza o *merge* de vários perfis e roda o CONCOCT para calcular os diferentes bins. Porém, como estamos analisando apenas um bin, não é necessário fazer tal merge, mas precisamos dizer pro Anvio que todos os contigs existentes no banco de dados pertencem ao mesmo bin. Para isso rodamos o script *06_import_binning.py*. O script faz duas coisas:

1. Cria um arquivo *.txt* onde cada linha corresponde a um contig de *contigsFile* e um nome do bin a que ele pertence, que por padrão é *contigsFile* removendo a extensão *.fasta* e os pontos ("."). 
2. Chama *anvi-import-collection* para falar pro Anvio que todos os contigs pertencem ao mesmo bin, e chama a coleção como sendo *contigsFile* removendo a extensão *.fasta* e os pontos (".").

O script é o seguinte:


```
#!python
import sys, os
from config import *
from createBinningTxt import *

srcPath     = os.path.join(contigsDir, contigsFile)
profilePath = os.path.join(outDir, "profile/PROFILE.db")
binFile     = os.path.join(outDir, contigsFile.replace(".fasta", ".txt"))
contigsDb   = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
collection  = contigsFile.replace(".fasta", "").replace(".", "")

createBinningFile(srcPath, binFile)

c = "anvi-import-collection %s -p %s -c %s -C %s --contigs-mode" %(binFile, profilePath, contigsDb, collection)
print "Running: ", c
os.system(c)
```

---

**Passo 7**. Resumo dos dados. Para criar um resumo dos dados do contig, rodar o script *07_summarize.py* que automaticamente passa os parâmetros necessários para o Anvio:

```
#!python
import sys, os
from config import *

profilePath = os.path.join(outDir, "profile/PROFILE.db")
contigsDb   = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
collection  = contigsFile.replace(".fasta", "").replace(".", "")
summaryDir  = os.path.join(outDir, "summary")
c = "anvi-summarize -p %s -c %s -o %s -C %s"%(profilePath, contigsDb, summaryDir, collection)
print "Running: ", c
os.system(c)
```

O arquivo com o resumo dos dados é criado em *outDir/summary*. O arquivo *index.html* mostra uma página web com links para várias informações sobre o bin.

---

**Passo 8**. Rodar a interface interativa de refinamento do Anvio. Para isso rodar o script *08_refining.py* que abrirá uma nova página no navegador mostrando a interface web de refinamento do Anvio. Os detalhes sobre como usar tal interface não estão incluídos nesse tutorial. O código é o seguinte:


```
#!python
import sys, os
from config import *

profilePath = os.path.join(outDir, "profile/PROFILE.db")
contigsDb   = os.path.join(outDir, contigsFile.replace(".fasta", ".db"))
collection  = contigsFile.replace(".fasta", "").replace(".", "")
binName     = contigsFile.replace(".fasta", "").replace(".", "")
c = "anvi-refine -p %s -c %s -C %s -b %s"%(profilePath, contigsDb, collection, binName)
print "Openning anvio refining interactive"
print "Running: ", c
os.system(c)
```

Lembrando que por simplicidade, a coleção é chamada *contigsFile* removendo a extensão *.fasta* e todos os pontos ("."), o bin é chamado da mesma forma (igual à coleção). Um exemplo do resultado é mostrado a seguir:

![Exemplo de resultado](https://bitbucket.org/lbi-usp/anvio_refine_bin/raw/master/resultado.png)

Dúvidas ou problemas: [diaztula@iq.usp.br](mailto:diaztula@iq.usp.br)