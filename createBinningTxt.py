import sys, os

def createBinningFile(srcFile, outFile):
    fout = open(outFile, "wt")
    fin  = open(srcFile, "rt")
    binName = srcFile[srcFile.rfind("/")+1:].replace(".fasta", "").replace(".", "")
    for l in fin:
        if l.startswith(">"):
            fout.write("%s\t%s\n"%(l[1:].strip(), binName))
